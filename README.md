# feeding-tube-3d-printables

3D printable pieces to help with gastrostomy tube ("G-tube") feeding.

These are a variety of 3D models and printable projects for people to use when
they have to do tube feeding. Files named `.slvs` are [SolveSpace][] 3.0 files -
the final version of 3.0 hasn't been released as of this writing, but the master
branch of SolveSpace is what I've been using, and is a major improvement over
2.2. SolveSpace is a completely free and open source, cross-platform parametric
constraint-based CAD program. I use it for most of my 3D printing work.

Note that unless you're taking many special preparations, 3D printed items are
**not food safe**. However, none of these models should ordinarily be used in a
way that would require them to be food safe.

## Printable projects

These are 3D printable models intended for use. (Contrast with the files in the
final section, which are used as references, linked into these models but not
directly printed.)

### Extension Tube End Handle/Grip

Thingiverse link: <https://www.thingiverse.com/thing:4181752>

You might want a little extra grip on the ENFit connector of an extension tube
if you, for instance, need to partially unscrew (then re-screw) a syringe to be
able to pull it back out after pushing in a bolus of formula. This printable
provides that.

It's designed for the ENFit connector on an AMT brand bolus (not y-port)
extension tube. That's the only kind of ENFit extension tube I have access to,
so no idea how other manufacturers' connectors compare. It might be usable with
them, or might need adjustments.

Be careful not to over-tighten, since this will give you extra leverage. Also,
you probably want to take it off before using with an overnight pump feeding,
lest you feed the bed.

You should just be able to squish the tube a little bit to get it to pop right
on, then slide it up to surround the ENFit connector. You can use the clamp to
keep the handle/grip up at the top.

- CAD file: `tube-end-handle.slvs`
  - Uses `enfit-ext-tube-end.slvs`, a quick model of the tube end in question,
    as the basis for the design.
- Printable: `tube-end-handle.stl`
  - Should load in the correct orientation. No supports required. It's pretty
    small so you could even print at a fairly high resolution and not take too
    long. My copy was printed in eSun PLA-PRO, in Cool White, and with 0.2mm
    layer height, but material and settings should not matter much: print in
    whatever you're familiar with.

![A view of the CAD model in SolveSpace](tube-handle-pics/cad-small.png)

![The model shown in PrusaSlicer, in preview mode](tube-handle-pics/sliced.png)

![The handle/grip in place on a feeding extension tube with bolus syringe](tube-handle-pics/on-tube-small.jpg)

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span
xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Feeding Tube 3D
Printables</span> by <a xmlns:cc="http://creativecommons.org/ns#"
href="https://gitlab.com/ryanpavlik/feeding-tube-3d-printables"
property="cc:attributionName" rel="cc:attributionURL">Ryan Pavlik</a> is
licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution
4.0 International License</a>. (CC-BY-4.0)

This provides permission to use, modify, and distribute freely, as long as
attribution is retained.

## Disclaimer/Trademarks

None of this is medical advice, just sharing of resources by someone who has
been there. If you have medical questions, go to your doctor.

ENFit is a federally registered trademark of GEDSA in multiple jurisdictions
throughout the world. Other names may be trademarks of their respective owners.

## Additional links

- I recommend supporting the
  [Feeding Tube Awareness Foundation](https://www.feedingtubeawareness.org/) and
  participating in their group.

[SolveSpace]: http://solvespace.com
[ENFit]: http://stayconnected.org/enteral-enfit/
